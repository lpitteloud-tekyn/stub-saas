<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class BaseController extends AbstractController
{
    /**
     * @param array|object $data 
     */
    public function sendJson($data, $statusCode = 200)
    {
        $message = null;
        $headers =[
            'X-Powered-By' => 'Hopes and Dreams',
            'X-Spirit-Animal' => 'Honeybadger'];

        $response = new JsonResponse($data, $statusCode, $headers);
        
        return $response;
    }

    /**
     * @param array|object $data
     */
    public function dump($data)
    {
        exit('<pre>'.print_r($data,1).'</pre>');
    }
}
