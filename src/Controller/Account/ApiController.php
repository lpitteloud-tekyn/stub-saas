<?php

namespace App\Controller\Account;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

use App\Service\UtilService;

use App\Entity\User;
use App\Entity\ApiKey;
use App\Entity\Account;
use App\Entity\AccountFeature;


/** 
 * @Route("/account/api", name="account_api_") 
 */
class ApiController extends AbstractController
{
   /**
    * Verify that this account has this feature enabled
    */
    private function featureEnabled()
    {
        $repo = $this->getDoctrine()->getRepository(AccountFeature::class);
        $enabled = $repo->apiEnabled($this->getUser()->getAccount());
        if (!$enabled instanceof AccountFeature) {
            throw new HttpException(403, 'You do not have this feature enabled, contact your administrator. Account Id:' . $this->getUser()->getAccount()->getId());
        }
    }

    /**
     * @Route("/list", name="list")
     */
    public function list()
    {
        
        $this->featureEnabled();
        
        $repo       = $this->getDoctrine()->getRepository(ApiKey::class);
        $keys       = $repo->findByAccount($this->getUser()->getAccount());
        
        return $this->render('Account/api/list.html.twig', [
            'keys' => $keys,
        ]);
    }

    /**
     * @Route("/create", name="create", methods={"POST"})
     */
    public function create(Request $request)
    {
        $name = $request->request->get('name');
        if (empty($name)) {
            $this->addFlash('danger', 'You must specify a name for your API Key');
        }

        // Need to get the user with the ROLE_API / and account to setup this key
        $repo       = $this->getDoctrine()->getRepository(User::class);
        $apiUser    = $repo->getApiUser($this->getUser()->getAccount());

        // Create the API User if it doesn't exist
        if (!$apiUser instanceof User) {
            $apiUser = new User();
            $apiUser->setRoles(["[ROLE_API]"]);
            $apiUser->setEmail(\uniqid() .'@apiUser.com');
            $apiUser->setPassword(\uniqid());
            $apiUser->setIsActive(true);
            $apiUser->setAccount($this->getUser()->getAccount());
    
            $this->getDoctrine()->getManager()->persist($apiUser);
            $this->getDoctrine()->getManager()->flush();
        }

        $util = new UtilService();
        $token = $util->generateToken($apiUser);

        $key = new ApiKey();
        $key->setName($name);
        $key->setUser($apiUser);
        $key->setAccount($this->getUser()->getAccount());
        $key->setToken($token);

        $this->getDoctrine()->getManager()->persist($key);
        $this->getDoctrine()->getManager()->flush();

        $this->addFlash('success', 'For security this is the <u>only</u> time we will show you the API Key<br/><br/><b>' . $token . '</b><br/>');

        return $this->redirectToRoute('account_api_list');
    }


    /**
     * @Route("/{apiKey}/edit", name="edit", methods={"POST"})
     */
    public function edit(ApiKey $apiKey, Request $request)
    {
        $this->featureEnabled();

        if ($apiKey->getAccount() != $this->getUser()->getAccount()) {
            throw new HttpException(400);
        }

        if ($request->isMethod("POST")) {
            $name   = $request->request->get('name');
            $active = (bool) $request->request->get('active');

            if (empty(trim($name))) {
                $this->addFlash('danger', 'The name of the key is required');
            } else {
                $apiKey->setName($name);
                $apiKey->setIsActive($active);
                        
                $this->getDoctrine()->getManager()->persist($apiKey);
                $this->getDoctrine()->getManager()->flush();
            }
        }

        return $this->redirectToRoute('account_api_list');
    }


    /**
     * @Route("/{apiKey}/delete", name="delete", methods={"POST"})
     */
    public function delete(ApiKey $apiKey)
    {
        $this->featureEnabled();
        if ($apiKey->getAccount() != $this->getUser()->getAccount()) {
            throw new HttpException(400);
        }

        $this->getDoctrine()->getManager()->remove($apiKey);
        $this->getDoctrine()->getManager()->flush();
        
        return $this->redirectToRoute('account_api_list');
    }
   
}
