<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Service\UtilService;
use App\Entity\User;
use App\Entity\Account;
use App\Entity\UserNote;
use App\Entity\AccountFeature;
use App\Entity\ApiKey;


class UserFixtures extends Fixture
{
    private $passwordEncoder;
    private $utilService;

    public function __construct(UtilService $utilService, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->utilService = $utilService;
    }

    public function load(ObjectManager $manager)
    {
        ////
        // Test General User
        ////
        $account1 = new Account();
        $account1->setIsActive(1);
        $account1->setName("Oscorp");
        $account1->setWebsite('https://marvel.fandom.com/wiki/Oscorp_(Earth-616)');
        $account1->setAllowedDomains('marvel.fandom.com, fandom.com');
        
        $user = new User();
        $user->setIsActive(true);
        $user->setIsVerified(true);
        $user->setIsDeleted(false);
        $user->setRoles(['ROLE_USER']);
        $user->setEmail('user@example.com');
        $user->setFirstName('John');
        $user->setLastName('Doe');
        $user->setPassword($this->passwordEncoder->encodePassword(
             $user,
             'userpassword'
        ));

        $user->setAccount($account1);
        $manager->persist($user);

        
        $account1->addUser($user);
        $manager->persist($account1); 

        ////
        // Test General User 2 belongs to same account
        ////
        $user2 = new User();
        $user2->setIsActive(true);
        $user2->setIsVerified(true);
        $user2->setIsDeleted(false);
        $user2->setRoles(['ROLE_USER']);
        $user2->setFirstName('Don');
        $user2->setLastName('Johnson');
        $user2->setEmail('user2@example.com');
        $user2->setPassword($this->passwordEncoder->encodePassword(
            $user2,
            'user2password'
        ));

        $user2->setAccount($account1);
        $manager->persist($user2);

        $account1->addUser($user2);
        $manager->persist($account1);

        ////
        // Test Admin User 
        ////
        $account = new Account();
        $account->setIsActive(1);
        $account->setName("Stark Industries");
        $account->setWebsite('https://starkindustries.com');
        $account->setAllowedDomains('starkindustries.com, ironman.com');
        

        $admin = new User();
        $admin->setIsActive(true);
        $admin->setIsVerified(true);
        $admin->setIsDeleted(false);
        $admin->setRoles(['ROLE_ADMIN']);
        $admin->setFirstName('Agent');
        $admin->setLastName('Smith');
        $admin->setEmail('admin@example.com');
        $admin->setPassword($this->passwordEncoder->encodePassword(
            $admin,
            'adminpassword'
        ));

        $admin->setAccount($account);
        $manager->persist($admin);

        $account->addUser($admin);
        $manager->persist($account);

        ////
        // Create notes 
        ////
        $note = new UserNote();
        $note->setUser($user);
        $note->setCreatedBy($admin);
        $note->setCreatedAt(new \DateTime());
        $note->setNote('This is a note created by the fixtures that is marked for a follow-up. Also, tacos sound really good right now.');
        $note->setRequireFollowUp(true);
        $manager->persist($note);

        $note1 = new UserNote();
        $note1->setUser($user);
        $note1->setCreatedBy($admin);
        $note1->setCreatedAt(new \DateTime());
        $note1->setRequireFollowUp(false);
        $note1->setNote('Called customer, Aaron mentioned that the check is in the mail. Still need tacos.');
        $manager->persist($note1);


        ////
        // Create API User and settings
        ////
        $apiUser = new User();
        $apiUser->setAccount($account1);
        $apiUser->setRoles(['ROLE_API']);
        $apiUser->setIsVerified(false);
        $apiUser->setIsActive(true);
        $apiUser->setIsDeleted(false);
        // Email can't be blank
        $apiUser->setEmail('null@null');
        // Password wouldn't work but can't be blank
        $apiUser->setPassword(\uniqid());
        $manager->persist($apiUser);

        // Enable API access on this account
        $feature = new AccountFeature();
        $feature->setAccount($account1);
        $feature->setName('apiEnabled');
        $feature->setValue(true);
        $manager->persist($feature);
 
        // Setup API Key
        $apiToken = $this->utilService->generateToken($user);

        $apiKey = new ApiKey();
        $apiKey->setAccount($account1);
        $apiKey->setToken($apiToken);
        $apiKey->setUser($apiUser);
        $apiKey->setCreatedAt(new \DateTime());
        $apiKey->setName('Test Key');
        $apiKey->setIsActive(true);
        $manager->persist($apiKey);


        // Write to db
        $manager->flush();
    }

}
