<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

use App\Entity\User;
use App\Entity\Account;
use App\Entity\ApiKey;


class UtilService 
{

    public function __construct()
    {

    }

    /**
     * @param User $user
     * @return string $token
     * 
     * Generates a random string to be used as the API Token
     */
    public function generateToken(User $user)
    {
        $salt       = $user->getId() . bin2hex(random_bytes(50));
        $accountId  = $user->getAccount()->getId();
        $hash       = hash_hmac('sha256', $salt, $accountId, true);
        $base       = preg_replace('/[^a-zA-Z0-9_\-]/','-', str_replace('=', '', base64_encode($hash)));
        $n          = mt_rand(4,8);
        $x          = str_split($base, $n);
        $glue       = ['.', '_'];
        $token      = 'AC._';
        
        foreach ($x as $i) {
            $token .= $i . $glue[rand(0,1)];
        }

        return $token .'_.DC';
    }
}